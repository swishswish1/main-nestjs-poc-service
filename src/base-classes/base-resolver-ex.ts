import { isFetchFromSql } from '../common/utils';
import { ISqlTransaction } from 'sql-base-lib';
const _ = require('lodash');

export class BaseResolverEx<Sql extends ISqlTransaction, Neo> /*extends BaseResolver*/ {
  protected service: any;

  protected constructor(protected sqlService: Sql, protected neoService: Neo) {
    this.service = isFetchFromSql ? sqlService : neoService
  }

  protected async modifyDb(items: any[], sqlFn: Function, neoFn: Function): Promise<string> {
    const result = await sqlFn(items);
    if (result.isOK) {
      const resultNeo = await neoFn(items);
      result.isOK = resultNeo.isOK;
      result.message += resultNeo.message;
    }
    else
      result.message += 'Neo was not called due to SQL rollback. ';

    await this.sqlService.endTransaction(result);
    return result.message;
  }

  // protected testPrint(text: string, ...arr: any[]) {
  //   logger.log(`testPrint: ${text}`);
  //   for (let i = 0; i < arr.length; i++)
  //     if (arr[i])
  //       logger.log(`${arr[i].id}`);
  // }
}
