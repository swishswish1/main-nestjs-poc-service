import { Inject, Module, Logger, LoggerService } from '@nestjs/common';
import { ZBClient } from 'zeebe-node/dist';
import { ZEEBE_CONNECTION_PROVIDER, ZeebeModule, ZeebeServer } from '@payk/nestjs-zeebe/dist';
import { RabbitMQModule } from '../rabbitmq/rabbitmq.module';
import { ZeeService } from './zee.service';
import { SqlService } from '../sql/sql.service';
import { ConfigModule, ConfigService } from 'config-lib';
import { logger } from 'logger-lib';
import { SqlModule } from '../sql/sql.module';
import { AuthModule } from 'auth-lib';
import { ZeeController } from './zee.controller';

@Module({
  imports: [
    ConfigModule,
    AuthModule,
    ZeebeModule.forRoot({
      gatewayAddress: new ConfigService().get('ZEEBE_GATEWAY_ADDRESS'),
      options: { loglevel: 'INFO', longPoll: 30000 },
    }),
    RabbitMQModule,
    SqlModule,
  ],
  controllers: [ZeeController],
  providers: [Logger, SqlService, ZeebeServer, ZeeService],
  exports: [ZeeService],
})
export class ZeeModule {
  constructor(
    configService: ConfigService,
    @Inject(ZEEBE_CONNECTION_PROVIDER) private readonly zbClient: ZBClient,
  ) {
    this.zbClient
      .deployWorkflow([
        `./bpmn/${configService.get('ZEEBE_WORKFLOW_1')}.bpmn`,
        `./bpmn/${configService.get('ZEEBE_WORKFLOW_2')}.bpmn`,
      ])
      .then(res => {
        logger.log('Workflow deployed:');
        logger.log(res);
      });
  }
}
