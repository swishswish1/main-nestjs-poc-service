// Note:
// file
// ...\node_modules\@nestjs\microservices\context\rpc-context-creator.js
// should be replaced with appropriate file from previous version provided in root directory.

import { Request, Response } from 'express';
import {
  Inject,
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseInterceptors,
  UseGuards,
  Param,
} from '@nestjs/common';
import { ZBClient } from 'zeebe-node';
import {
  ZEEBE_CONNECTION_PROVIDER,
  ZeebeWorker,
  ZeebeServer,
} from '@payk/nestjs-zeebe';
import { Consumer } from 'rabbitmq-provider/consumer';
import { RabbitmqConsumerService } from '../rabbitmq/rabbitmq-consumer.service';
import { BaseZeebeController } from '../../base-classes/base-zeebe.controller';
import { ConfigService } from 'config-lib';
import { logger } from 'logger-lib';
import { ZeeService } from './zee.service';
import { AuthService, LocalAuthGuard, JwtAuthGuard } from 'auth-lib';
import { jsonStr } from 'common-utils-lib';
import { ApiImplicitParam } from '@nestjs/swagger/dist/decorators/api-implicit-param.decorator';
import { ApiBearerAuth } from '@nestjs/swagger';
const { LogDurationInterceptor, DurationInterceptor } = require('interceptors-lib');

//@ApiBearerAuth()
@Controller()
export class ZeeController extends BaseZeebeController {
  workflowInstance: any;

  constructor(
    private readonly configService: ConfigService,
    @Inject(ZEEBE_CONNECTION_PROVIDER) zbClient: ZBClient,
    rabbitmqConsumerService: RabbitmqConsumerService, // gateway consumer
    // private readonly authService: AuthService,
    // zeebeServer: ZeebeServer,
    // private zeeService: ZeeService,
  ) {
    super(zbClient, rabbitmqConsumerService, (consumer, messages) => {
      const payloads = Consumer.getPayloads(messages);
      payloads.forEach(item => this.sendResultToClient(item.sessionId, item.result));
    });
  }

  // Use the client to create a new workflow instance
  @Get('/1')
  @UseInterceptors(DurationInterceptor)
  @UseInterceptors(new LogDurationInterceptor(logger))
  async createWorkflow1(@Req() req: Request, @Res() res: Response) {
    logger.log('*** createWorkflow1');
    await this.createWorkflow(this.configService.get('ZEEBE_WORKFLOW_1'), res);
  }

  // ZeebeWorker 1

  // // Subscribe to events of type 'task-1-1' and
  // //   create a worker with the options as passed below (zeebe-node ZBWorkerOptions)
  // @ZeebeWorker('task-1-1')
  // async task11(job, complete) {
  //   await this.runTask(job, complete, this.zeebeService.task1);
  // }
  //
  // @ZeebeWorker('task-1-2')
  // async task12(job, complete) {
  //   await this.runTask(job, complete, this.zeebeService.task2, 3);
  // }

  // @ZeebeWorker('task-1-3')
  // async task13(job, complete) {
  //   await this.runTask(job, complete, this.zeebeService.task3, 1);
  // }
  //
  // @ZeebeWorker('task-1-4')
  // async task14(job, complete) {
  //   await this.runTask(job, complete, this.zeebeService.task4, 1);
  // }
}
