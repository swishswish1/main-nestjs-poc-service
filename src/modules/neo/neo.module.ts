import { Module } from '@nestjs/common';
import { NeoService } from './neo.service';
import { ConfigModule } from 'config-lib';

@Module({
  imports: [ConfigModule],
  providers: [NeoService],
  exports: [NeoService],
})
export class NeoModule {}
