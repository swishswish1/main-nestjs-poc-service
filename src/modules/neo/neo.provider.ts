import { Connection } from 'cypher-query-builder';
import { Injectable, Logger } from '@nestjs/common';
import { logger } from 'logger-lib';

@Injectable()
export class Db extends Connection {
  constructor(url: string, auth: any) {
    super(url, auth);
    this.options.driverConfig.encrypted = false;
  }

  writeTransaction = async (fn: Function): Promise<any> => {
    let isOK = true;
    let message = '';
    const session = this.driver.session();
    await session.writeTransaction(async tx => {
      try {
        message = await fn(tx);
        tx.commit();
        message = 'Neo transaction committed. ';
      } catch (err) {
        tx.rollback();
        isOK = false;
        message = `${err} Neo transaction rolled back. `;
      }
    });

    await session.close();
    return { isOK, message };
  };

  static alreadyExists = async (tx: any, id: string): Promise<boolean> =>
    (await tx.run(`MATCH (p) WHERE (p.id = "${id}") RETURN (p)`)).records
      .length > 0;

  emptyDb = async () => {
    // Removing previous records
    let deletion = 'MATCH (n) DETACH DELETE (n)';
    try {
      await this.raw(deletion).run();
    } catch (err) {
      logger.error(err);
    }
  };
}
