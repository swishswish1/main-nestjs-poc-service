import { Injectable, Logger, LoggerService } from '@nestjs/common';
import { Consumer } from 'rabbitmq-provider/consumer';
import { ConfigService } from 'config-lib';
import { ServiceInstanceId } from 'common-utils-lib';
import { logger } from 'logger-lib';

@Injectable()
export class RabbitmqConsumerService {
  consumer: Consumer;

  async createAndStartProcessing(fnConsume: Function) {
    this.consumer = await Consumer.createConsumer(
      {
        connUrl: new ConfigService().get('RABBITMQ_URL'),
        queue: ServiceInstanceId.getId(),
        noAck: true,
      },
      logger,
      fnConsume,
    );
  }
}
