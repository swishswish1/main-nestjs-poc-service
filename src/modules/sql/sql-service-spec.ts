import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../app.module';
import { INestApplication, Inject } from '@nestjs/common';
import { SqlService } from './sql.service';
import { SqlModule } from './sql.module';
import { Connection } from 'typeorm';
import { goNDirsUp } from '../../common/utils';
// import * as request from 'supertest';

describe('Sql Tests', () => {
  let app: INestApplication;
  let moduleFixture: TestingModule;

  beforeAll(async () => {
    // const dir = 'C:\\prj\\SwishSwish\\graphql-neo4j\\dist';
    // process.chdir(dir);

    moduleFixture = await Test.createTestingModule({
      imports: [SqlModule],
      providers: [SqlService],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  beforeEach(async () => {
    //sqlService = new SqlService(new Connection({
    //}));
  });

  it('1st Sql test', async () => {
    //__dirname = undefined;
    const sqlService = moduleFixture.get('SqlService');
    const qq = await sqlService.allPersons();
    return qq;
  });

  // it('/ (GET)', () => {
  //   return request(app.getHttpServer())
  //     .get('/')
  //     .expect(200)
  //     .expect('Hello World!');
  // });

  afterAll(async () => {});
});
