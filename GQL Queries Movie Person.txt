query Persons {
	allPersons {
  	id
    givenName
    surname
    address
    affiliations {
      organization {
        name
        address
        parent {
          name
        }
      }
      role {
        name
        description
      }
    }
    relations {
      id
      p2 {
        givenName
        surname
      }
      kind
    }
  }
}

query PersonById {
  personById(id: "p_5") {
    givenName
    affiliations(
      organization: "IIT Kanpur",
    	role: "Director",
    	since: {
      	_range: {
      		_ge: 2002
      	}
      }
    ) {
      organization {
        name
        parent {
          name
        }
      }
      role {
        name
      }
      since
    }
    relations(kind: "manager") {
      p2 {
        givenName
      }
      kind
    }
  }
}

query PersonBySurname {
  personBySurname(surname: "Choudhury") {
    givenName
    surname
  }
}

query PersonsByRelation {
  personsByRelation(relationQueryArg: [
    {
    	kind: "colleague"
    	p1Id: "p_1"
    }
    {
    	kind: "colleague"
    	p1Id: "p_2"
    }
  ]) {
    id
    givenName
    surname
  }
}

query Organizations {
  allOrganizations {
    name
    parent {
      name
    }
  }
}

mutation Mutation {
	createPersons(personsInput: [
    {
      id: "p_4"
   	  givenName: "Sounak"
      surname: "Choudhury"
      born: 1991
      affiliations: [
        {
          id: "a_6"
          organizationId: "o_iitk_me"
          roleId: "r_pr"
          since: 2017
        }
      ]
    }
    {
      id: "p_5"
   	  givenName: "Sandjay"
      surname: "Dhande"
      born: 1980
      affiliations: [
        {
          id: "a_7"
          organizationId: "o_iitk_me"
          roleId: "r_pr"
          since: 2010
        }
        {
          id: "a_8"
          organizationId: "o_iitk"
          roleId: "r_dir"
          since: 2015
        }
      ]
      relations: [
        {
		  id: "l_1"
          p2Id: "p_1"
          kind: "manager"
          since: 2020
          notes: "a"
        }
        {
		  id: "l_2"
          p2Id: "p_2"
          kind: "manager"
          since: 2020
          notes: "b"
        }
      ]
    }
  ])
}




MERGE (MarkoFeretti:Person{id:"p_10", givenName:"Marko", surname:"Feretti", born:2002, phone:"1234567", email:"markof@ua.ac.it", address:"qwerty"})


MATCH (person) WHERE (person.id = "p_10")
MATCH (org) WHERE (org.id = "o_iitk")
MATCH (role) WHERE (role.id = "r_st")
MERGE (Aff100:Affiliation{id:"a_100", since:2020})
MERGE (Aff100)-[:HAS]->(org)
MERGE (Aff100)-[:HAS]->(role)
MERGE (person)-[:HAS]->(Aff100)


MATCH (person) WHERE (person.id = "p_10")
MATCH (p2:Person) WHERE (p2.id = "p_1")
MERGE (L1:Relation {id:"l_100", kind:"manager", since:2020, notes:"xx", p1:person.id, p2:p2.id})
MERGE (person)-[:HAS]->(L1)
RETURN L1


MATCH (person) WHERE (person.id = "p_10")
MATCH (p2:Person) WHERE (p2.id = "p_1")
MERGE (L1:Relation {id:"l_100", kind:"manager", since:2020, notes:"xx", p1:person.id, p2=p2.id})
MERGE (L1)-[:RELTYPE { has: L1.p1 = person.id }]->(person)
MERGE (L1)-[:RELTYPE { has: L1.p2 = p2.id }]->(p2)
MERGE (person)-[:HAS]->(L1)
RETURN L1






CREATE ( TheMartix:Movie{id:"1", title:"The Martix", year:1999})
CREATE ( AFewGoodMen:Movie{id:"2", title:"A Few Good Men", year:1992})
CREATE ( TopGun:Movie{id:"3", title:"Top Gun", year:1986})
CREATE ( Keanu:Actor{id:"4", name:"Keanu Reeves", born:1964})
CREATE ( Carrie:Actor{id:"5", name:"Carrie-Anne Moss", born:1967})
CREATE ( Laurence:Actor{id:"6", name:"Laurence Fishburne", born:1961})
CREATE ( Hugo:Actor{id:"7", name:"Hugo Weaving", born:1960})
CREATE (TomC:Actor {id:"8", name:'Tom Cruise', born:1962})
CREATE (JackN:Actor {id:"9", name:'Jack Nicholson', born:1937})
CREATE (DemiM:Actor {id:"10", name:'Demi Moore', born:1962})
CREATE (KevinB:Actor {id:"11", name:'Kevin Bacon', born:1958})
CREATE (RiverP:Actor {id:"12", name:'River Phoenix', born:1970})
CREATE (CoreyF:Actor {id:"13", name:'Corey Feldman', born:1971})
CREATE (WilW:Actor {id:"14", name:'Wil Wheaton', born:1972})
CREATE (JohnC:Actor {id:"15", name:'John Cusack', born:1966})
CREATE (KellyM:Actor {id:"16", name:'Kelly McGillis', born:1957})
CREATE (ValK:Actor {id:"17",name:'Val Kilmer', born:1959})
CREATE
  (Keanu)-[:ACTED_IN] ->(TheMatrix),
  (Carrie)-[:ACTED_IN]->(TheMatrix),
  (Laurence)-[:ACTED_IN]->(TheMatrix),
  (Hugo)-[:ACTED_IN]->(TheMatrix),
  (TomC)-[:ACTED_IN ]->(AFewGoodMen),
  (JackN)-[:ACTED_IN ]->(AFewGoodMen),
  (DemiM)-[:ACTED_IN ]->(AFewGoodMen),
  (KevinB)-[:ACTED_IN]->(AFewGoodMen),
  (TomC)-[:ACTED_IN]->(TopGun),
  (KellyM)-[:ACTED_IN]->(TopGun)
CREATE
 (TheMatrix)-[:HAS]->(Keanu),
 (TheMatrix)-[:HAS]->(Carrie), 
 (TheMatrix)-[:HAS]->(Laurence), 
 (TheMatrix)-[:HAS]->(Hugo),
 (AFewGoodMen)-[:HAS]->(TomC),  
 (AFewGoodMen)-[:HAS]->(JackN),  
 (AFewGoodMen)-[:HAS]->(KevinB), 
 (TopGun)-[:HAS]->(TomC),  
 (TopGun)-[:HAS]->(KellyM)
